var express = require('express');
var app = express();
var router = express.Router();

app.set("view engine", "ejs");

app.get('/views', function(req, res){
	res.sendFile(__dirname + "/index.ejs")
});
app.get('/', function(req,res){
	res.render("index.ejs", {maincontext:"Assignment #9: Server-side JavaScript I - Phuong Nguyen"})
});
app.use(express.static(__dirname + '/public'));

app.get('/page1', function(req, res){
	res.sendFile(__dirname + "/page1.html")
});
app.get('/1', function(req, res){
	res.sendFile(__dirname + "/page1.html")
});
app.get('/one', function(req, res){
	res.sendFile(__dirname + "/page1.html")
});
app.get('/page2', function(req, res){
	res.sendFile(__dirname + "/page2.html")
});
app.get('/2', function(req, res){
	res.sendFile(__dirname + "/page2.html")
});
app.get('/two', function(req, res){
	res.sendFile(__dirname + "/page2.html")
});
app.get('/page3', function(req, res){
	res.sendFile(__dirname + "/page3.html")
});
app.get('/3', function(req, res){
	res.sendFile(__dirname + "/page3.html")
});
app.get('/three', function(req, res){
	res.sendFile(__dirname + "/page3.html")
});

app.get('*', function(req, res, next) {
  var err = new Error();
  err.status = 404;
  next(err);
});
 
// handling 404 errors
app.use(function(err, req, res, next) {
  if(err.status !== 404) {
    return next();
  }
 
  res.send( err.message || '* That’s an error for bad requests. *');
});

app.listen(3000);